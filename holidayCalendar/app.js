//app.js
App({
  onLaunch: function () {
    this.globalData = {}
  },

  getCacheSync(_key){
    return wx.getStorageSync(_key);
  },

  //获取App背景设置
  getTheme: function() {
    var bg = this.getCacheSync("theme");
    if (!bg) {
      bg = "background:#2CA8E8 url(http://h5.sumslack.com/wx/bg.png) repeat fixed top"
    };
    return bg;
  },

})

